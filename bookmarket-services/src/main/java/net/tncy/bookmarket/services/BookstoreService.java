package net.tncy.bookmarket.services;

import net.tncy.bookmarket.data.Bookstore;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class BookstoreService {

    private EntityManager em;

    public List<Bookstore> findAll(){
        Query query = em.createQuery("select * from BOOKSTORES");
        return query.getResultList();
    }
}
