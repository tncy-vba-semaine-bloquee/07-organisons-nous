package net.tncy.bookmarket.services;

import net.tncy.bookmarket.data.Book;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class BookService {

    private EntityManager em;

    public List<Book> findAll(){
        Query query = em.createQuery("select * from BOOKS");
        return query.getResultList();
    }
}
