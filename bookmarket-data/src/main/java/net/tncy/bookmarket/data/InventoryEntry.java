package net.tncy.bookmarket.data;

public class InventoryEntry {

    private Book book;

    private int quantity;

    private float suggestedPrice;

    private float price;

    public InventoryEntry(Book book, int quantity, float suggestedPrice, float price) {
        this.book = book;
        this.quantity = quantity;
        this.suggestedPrice = suggestedPrice;
        this.price = price;
    }

    public void addBookLot(int lotSize, float unitPrice) {
        this.suggestedPrice = (this.quantity * this.suggestedPrice + lotSize * unitPrice) / (this.quantity + lotSize);
        this.quantity += lotSize;
    }

    public void removeBookLot(int lotSize) {
        this.quantity -= lotSize;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getsuggestedPrice() {
        return suggestedPrice;
    }

    public void setsuggestedPrice(float suggestedPrice) {
        this.suggestedPrice = suggestedPrice;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

}
