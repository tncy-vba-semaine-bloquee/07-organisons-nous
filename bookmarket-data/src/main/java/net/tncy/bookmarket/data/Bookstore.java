package net.tncy.bookmarket.data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "BOOKSTORES")
public class Bookstore {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private String name;

    private final List<InventoryEntry> inventory;

    public Bookstore() {
        this.inventory = new ArrayList<>();
    }

    public Bookstore(int id, String name) {
        this();
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<InventoryEntry> getInventory() {
        return inventory;
    }

    public void addBookLot(Book book, int lotSize, float unitPrice) {
        InventoryEntry entry = inventoryEntryLookup(book.getId());
        if (entry != null) {
            entry.addBookLot(lotSize, unitPrice);
        } else {
            entry = new InventoryEntry(book, lotSize, unitPrice, unitPrice);
            inventory.add(entry);
        }
    }

    public InventoryEntry inventoryEntryLookup(Integer bookId) {
        // TODO
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
